#!/bin/bash
sudo mkdir -p /home/ubuntu/code
cd /home/ubuntu/code
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/amr-common.git
cd /home/ubuntu/code/amr-common
sudo ./gradlew assemble install -x test
cd /home/ubuntu/code
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/amr-ingestion-pipeline.git
cd /home/ubuntu/code/amr-ingestion-pipeline
sudo ./gradlew assemble -x test
cd /home/ubuntu/code/amr-ingestion-pipeline/provision
sudo docker-compose build --force-rm --no-cache
sudo docker-compose up -d
echo "52.57.37.147  amradmin" | sudo tee -a /etc/hosts
