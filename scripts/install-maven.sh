#!/bin/bash

sudo curl -O http://mirrors.ibiblio.org/apache/maven/maven-3/3.2.5/binaries/apache-maven-3.2.5-bin.tar.gz
sudo tar xzf apache-maven-3.2.5-bin.tar.gz -C /home/ubuntu
echo "export PATH=$PATH:/home/ubuntu/apache-maven-3.2.5/bin" | sudo tee -a /home/ubuntu/.bashrc
sudo chown -R $(whoami) /home/ubuntu/apache-maven-3.2.5
sudo rm -f apache-maven-3.2.5-bin.tar.gz
