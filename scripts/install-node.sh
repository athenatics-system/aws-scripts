#!/bin/bash
sudo apt-get update
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs
sudo ln -s /usr/bin/nodejs /usr/bin/node
sudo npm install npm -g
sudo npm install -g bower --allow-root
sudo npm install -g grunt-cli --allow-root
sudo chown -R $USER:$GROUP /home/ubuntu/.npm
sudo chown -R $USER:$GROUP /home/ubuntu/.config
echo '{ "allow_root": true }' > /home/ubuntu/.bowerrc
