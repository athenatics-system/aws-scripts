#!/bin/bash

cd /home/ubuntu/code

export PATH=$PATH:/home/ubuntu/apache-maven-3.2.5/bin

sudo rm -rf amr-common
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/amr-common.git
cd /home/ubuntu/code/amr-common
sudo git checkout -b deployment v1.0.0
sudo ./gradlew assemble install -x test

sudo env "PATH=$PATH" mvn install:install-file -Dfile=/home/ubuntu/code/amr-common/amr-common-api/build/libs/amr-common-api-1.0.0-SNAPSHOT.jar \
 -DgroupId=com.amr -DartifactId=amr-common-api -Dversion=1.0.0 -Dpackaging=jar

 sudo env "PATH=$PATH" mvn install:install-file -Dfile=/home/ubuntu/code/amr-common/amr-common-config/build/libs/amr-common-config-1.0.0-SNAPSHOT.jar \
  -DgroupId=com.amr -DartifactId=amr-common-config -Dversion=1.0.0 -Dpackaging=jar

sudo env "PATH=$PATH" mvn install:install-file -Dfile=/home/ubuntu/code/amr-common/amr-common-model/build/libs/amr-common-model-1.0.0-SNAPSHOT.jar \
   -DgroupId=com.amr -DartifactId=amr-common-model -Dversion=1.0.0 -Dpackaging=jar

sudo env "PATH=$PATH" mvn install:install-file -Dfile=/home/ubuntu/code/amr-common/amr-common-service/build/libs/amr-common-service-1.0.0-SNAPSHOT.jar \
    -DgroupId=com.amr -DartifactId=amr-common-service -Dversion=1.0.0 -Dpackaging=jar

cd /home/ubuntu/code
sudo rm -rf ingestion-pipeline-topology
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/ingestion-pipeline-topology.git
cd /home/ubuntu/code/ingestion-pipeline-topology
sudo git checkout -b deployment v1.0.0
sudo env "PATH=$PATH" mvn clean install -Dmaven.test.skip=true


CONTAINER_ID=$(docker ps | grep athenaticsstormnimbus | cut -f1 -d' ')
echo $CONTAINER_ID
#log
echo "got the container id $CONTAINER_ID" | sudo tee -a /home/ubuntu/install.log

if [[ -z "$CONTAINER_ID" ]]
then
  echo "container id not retrievd from docker ps " | sudo tee -a /home/ubuntu/install.log
  exit 2;
fi

NIMBUS_IP=$(docker inspect --format '{{ .NetworkSettings.Networks.amrdeployment_default.IPAddress }}' $CONTAINER_ID)
echo $NIMBUS_IP

#log
echo "got the ip address of nimbus $NIMBUS_IP" | sudo tee -a /home/ubuntu/install.log

sudo sed -i "/$CONTAINER_ID/d" /etc/hosts
echo "$NIMBUS_IP  $CONTAINER_ID" | sudo tee -a /etc/hosts

export PATH=$PATH:/home/ubuntu/apache-storm-1.0.1/bin
#log
echo "killing tompology amr-ingestion " | sudo tee -a /home/ubuntu/install.log
storm kill ingestion-pipeline -c nimbus.host=$NIMBUS_IP -c nimbus.thrift.port=6627 -w 10

#log
echo "starting tompology amr-ingestion " | sudo tee -a /home/ubuntu/install.log
storm jar /home/ubuntu/code/ingestion-pipeline-topology/target/ingestion-pipeline-topology-1.0.0.jar \
com.athenatics.ingestion.topology.IngestionTopology aws -c nimbus.host=$NIMBUS_IP -c nimbus.thrift.port=6627
