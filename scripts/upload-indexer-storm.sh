#!/bin/bash


cd /home/ubuntu/code
sudo rm -rf amr-message-indexer
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/amr-message-indexer.git
cd /home/ubuntu/code/amr-message-indexer
sudo git checkout -b deployment v1.0.0
sudo ./gradlew assemble shadowJar -x test


CONTAINER_ID=$(docker ps | grep athenaticsstormnimbus | cut -f1 -d' ')
echo $CONTAINER_ID
#log
echo "got the container id $CONTAINER_ID" | sudo tee -a /home/ubuntu/install.log

if [[ -z "$CONTAINER_ID" ]]
then
  echo "container id not retrievd from docker ps " | sudo tee -a /home/ubuntu/install.log
  exit 2;
fi

NIMBUS_IP=$(docker inspect --format '{{ .NetworkSettings.Networks.amrdeployment_default.IPAddress }}' $CONTAINER_ID)
echo $NIMBUS_IP

#log
echo "got the ip address of nimbus $NIMBUS_IP" | sudo tee -a /home/ubuntu/install.log

sudo sed -i "/$CONTAINER_ID/d" /etc/hosts
echo "$NIMBUS_IP  $CONTAINER_ID" | sudo tee -a /etc/hosts

export PATH=$PATH:/home/ubuntu/apache-storm-1.0.1/bin

#log
echo "killing tompology message-indexer " | sudo tee -a /home/ubuntu/install.log
storm kill message-indexer -c nimbus.host=$NIMBUS_IP -c nimbus.thrift.port=6627 -w 10
sleep 10
#log
echo "starting tompology message-indexer " | sudo tee -a /home/ubuntu/install.log
storm jar /home/ubuntu/code/amr-message-indexer/amr-message-indexer-app/build/libs/amr-message-indexer-app-1.0.0-all.jar \
com.athenatics.message.indexer.Application aws -c nimbus.host=$NIMBUS_IP -c nimbus.thrift.port=6627
