#!/bin/bash
cd /home/ubuntu/code
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/amr-admin-ui.git
cd /home/ubuntu/code/amr-admin-ui
#sudo chmod +x gradlew
sudo ./gradlew -Pprod bootRepackage -x test
cd /home/ubuntu/code/amr-admin-ui/src/main/docker
sudo docker-compose build --force-rm --no-cache
sudo docker-compose up -d
