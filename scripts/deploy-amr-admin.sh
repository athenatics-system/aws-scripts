#!/bin/bash
sudo mkdir -p /home/ubuntu/code
cd /home/ubuntu/code
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/amr-common.git
cd /home/ubuntu/code/amr-common
sudo ./gradlew assemble install
cd /home/ubuntu/code
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/amr-admin.git
cd /home/ubuntu/code/amr-admin
sudo git checkout amr-admin-csv
sudo git pull
sudo ./gradlew assemble -x test
cd /home/ubuntu/code/amr-admin/amr-admin-app/provision/prod
sudo docker-compose -f prod.yml build --force-rm --no-cache
sudo docker-compose -f prod.yml up -d
