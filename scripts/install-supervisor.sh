#!/bin/bash
sudo mkdir -p /etc/supervisor/conf.d
sudo touch /etc/supervisor/conf.d/athenatics-supervisor.conf
echo '[inet_http_server]' | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "port = 8487" | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf

echo " " | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf

# supervisor indexer
echo '[program:upload-indexer]' | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "command=sudo bash /home/ubuntu/aws-scripts/scripts/upload-indexer-storm.sh" | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "directory=/home/ubuntu" | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "user=ubuntu " | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "autostart=false " | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "autorestart=false " | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "stderr_logfile=/home/ubuntu/upload-indexer-storm.err.log " | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "stdout_logfile=/home/ubuntu/upload-indexer-storm.out.log " | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf

echo " " | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf

# supervisor ingestion
echo '[program:upload-ingestion]' | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "command=sudo bash /home/ubuntu/aws-scripts/scripts/upload-ingestion-storm.sh" | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "directory=/home/ubuntu" | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "user=ubuntu " | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "autostart=false " | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "autorestart=false " | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "stderr_logfile=/home/ubuntu/upload-ingestion-storm.err.log " | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf
echo "stdout_logfile=/home/ubuntu/upload-ingestion-storm.out.log " | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf

echo " " | sudo tee -a /etc/supervisor/conf.d/athenatics-supervisor.conf

sudo apt-get install -y supervisor
sudo /usr/bin/supervisord -c /etc/supervisor/supervisord.conf
