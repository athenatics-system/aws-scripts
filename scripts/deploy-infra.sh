#!/bin/bash
cd /home/ubuntu/code
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/amr-deployment.git
cd /home/ubuntu/code/amr-deployment
sudo git checkout -b deployment v1.0.0
cd /home/ubuntu/code/amr-deployment/info
sudo find public-html/graphdb.html -type f -print0 | xargs -0 sudo sed -i 's/192.168.99.100/52.57.37.147/g'
sudo find public-html/mysql.html -type f -print0 | xargs -0 sudo sed -i 's/192.168.99.100/52.57.37.147/g'
sudo find public-html -type f -print0 | xargs -0 sudo sed -i 's/192.168.99.100/52.57.19.113/g'
cd /home/ubuntu/code/amr-deployment
sudo docker-compose -f athenatics-pipeline-compose.yml build --force-rm --no-cache
sudo docker-compose -f athenatics-pipeline-compose.yml up -d athenaticselasticsearch athenaticskibana athenaticsstormsupervisor athenaticsstormui athenaticskafka athenaticskafkamanager info
out=$(docker ps)
echo $out | sudo tee -a /home/ubuntu/install.log
