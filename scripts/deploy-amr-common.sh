#!/bin/bash
cd /home/ubuntu/code

sudo rm -rf amr-common
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/amr-common.git
cd /home/ubuntu/code/amr-common
sudo git checkout -b deployment v1.0.0
sudo ./gradlew assemble install

sudo env "PATH=$PATH" mvn install:install-file -Dfile=/home/ubuntu/code/amr-common/amr-common-api/build/libs/amr-common-api-1.0.0-SNAPSHOT.jar \
 -DgroupId=com.amr -DartifactId=amr-common-api -Dversion=1.0.0 -Dpackaging=jar

 sudo env "PATH=$PATH" mvn install:install-file -Dfile=/home/ubuntu/code/amr-common/amr-common-config/build/libs/amr-common-config-1.0.0-SNAPSHOT.jar \
  -DgroupId=com.amr -DartifactId=amr-common-config -Dversion=1.0.0 -Dpackaging=jar

sudo env "PATH=$PATH" mvn install:install-file -Dfile=/home/ubuntu/code/amr-common/amr-common-model/build/libs/amr-common-model-1.0.0-SNAPSHOT.jar \
   -DgroupId=com.amr -DartifactId=amr-common-model -Dversion=1.0.0 -Dpackaging=jar

sudo env "PATH=$PATH" mvn install:install-file -Dfile=/home/ubuntu/code/amr-common/amr-common-service/build/libs/amr-common-service-1.0.0-SNAPSHOT.jar \
    -DgroupId=com.amr -DartifactId=amr-common-service -Dversion=1.0.0 -Dpackaging=jar
