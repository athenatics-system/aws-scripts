#!/bin/bash
sudo mkdir -p /home/ubuntu/code
cd /home/ubuntu/code
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/amr-admin.git
cd /home/ubuntu/code/amr-admin
sudo git checkout amr-admin-csv
sudo git pull
sudo ./gradlew build install -x test
cd /home/ubuntu/code
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/amr-notifications.git
cd /home/ubuntu/code/amr-notifications
sudo ./gradlew clean build -x test
cd /home/ubuntu/code/amr-notifications/provision
sudo docker-compose build --force-rm --no-cache
sudo docker-compose up -d
