#!/bin/bash
sudo mkdir -p /home/ubuntu/code
cd /home/ubuntu/code
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/amr-searcher.git
cd amr-searcher
sudo ./gradlew clean install
cd /home/ubuntu/code
sudo git clone https://athenatics-system:passw0rd@bitbucket.org/amrathenatics/amr-dispatcher.git
cd amr-dispatcher
sudo ./gradlew clean assemble -x test
cd /home/ubuntu/code/amr-dispatcher/provision
sudo docker-compose build --force-rm --no-cache
sudo docker-compose up -d
