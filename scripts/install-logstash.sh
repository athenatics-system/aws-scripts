#!/bin/bash

sudo curl -O https://download.elastic.co/logstash/logstash/logstash-2.4.0.tar.gz
sudo tar xzf logstash-2.4.0.tar.gz -C /home/ubuntu
echo "export PATH=$PATH:/home/ubuntu/logstash-2.4.0/bin" | sudo tee -a /home/ubuntu/.bashrc
sudo chown -R $(whoami) /home/ubuntu/logstash-2.4.0
sudo rm -f logstash-2.4.0.tar.gz
