#!/bin/bash

sudo curl -O http://www.gtlib.gatech.edu/pub/apache/storm/apache-storm-1.0.1/apache-storm-1.0.1.tar.gz
sudo tar xzf apache-storm-1.0.1.tar.gz -C /home/ubuntu
echo "export PATH=$PATH:/home/ubuntu/apache-storm-1.0.1/bin" | sudo tee -a /home/ubuntu/.bashrc
sudo chown -R $(whoami) /home/ubuntu/apache-storm-1.0.1
sudo rm -f apache-storm-1.0.1.tar.gz
