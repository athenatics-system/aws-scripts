#!/bin/bash

cd aws-scripts/scripts
sudo mkdir -p /home/ubuntu/code
sudo chmod +x *.sh

#install infra
bash /home/ubuntu/aws-scripts/scripts/deploy-infra.sh

#install java
./install-java.sh
source /home/ubuntu/.bashrc

#install maven
bash /home/ubuntu/aws-scripts/scripts/install-maven.sh
source /home/ubuntu/.bashrc

#install ingestion,#sudo nohup ./deploy-amr-ingestion.sh > ~/installIngestion.log 2>&1 &
bash /home/ubuntu/aws-scripts/scripts/deploy-amr-ingestion.sh > ~/installIngestion.log 2>&1 &

#deploy amr dispatcher
bash /home/ubuntu/aws-scripts/scripts/deploy-amr-dipatcher.sh > ~/installDispatcher.log 2>&1 &

#deploy amr notification
bash /home/ubuntu/aws-scripts/scripts/deploy-amr-notification.sh > ~/installNotification.log 2>&1 &

#install storm,#sudo nohup ./install-storm.sh > ~/installstorm.log 2>&1 &
bash /home/ubuntu/aws-scripts/scripts/install-storm.sh
source /home/ubuntu/.bashrc
